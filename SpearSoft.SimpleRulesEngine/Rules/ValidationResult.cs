namespace SpearSoft.SimpleRulesEngine.Rules
{
    public class ValidationResult
    {
        public ValidationResult(bool isValid, Severity highestSeverity, string validationMessage)
        {
            IsValid = isValid;
            HighestSeverity = highestSeverity;
            ValidationMessages = validationMessage;
        }

        public bool IsValid { get; private set; }
        public Severity HighestSeverity { get; private set; }
        public string ValidationMessages { get; set; }
    }
}
