namespace SpearSoft.SimpleRulesEngine.Rules
{
    public interface IRule
    {
        bool IsValid { get; set; }
        Severity Severity { get; set; }
        RuleTypes RuleType { get; set; }
        string ValidationMessage { get; set; }

        IRule Execute(object target);
    }
}
