namespace SpearSoft.SimpleRulesEngine.Rules
{
    public enum Severity
    {
        None = 0,
        Information = 1,
        Warning = 2,
        Stop = 3
    }
}
