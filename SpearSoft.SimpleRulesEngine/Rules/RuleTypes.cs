namespace SpearSoft.SimpleRulesEngine.Rules
{
    public enum RuleTypes
    {
        WordLength = 0,
        LetterNotAllowed = 1,
        LetterRequired = 2,
        IsValidWord = 3,
        Contains = 4
    }
}
