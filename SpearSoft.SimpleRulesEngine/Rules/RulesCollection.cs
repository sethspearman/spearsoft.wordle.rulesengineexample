using System;
using System.Collections.Generic;

namespace SpearSoft.SimpleRulesEngine.Rules
{
    public class RulesCollection : List<IRule>
    {
        public ValidationResult CheckRules(object obj)
        {
            var isValid = true;
            var validationMessage = string.Empty;
            Severity highestSeverity = Severity.None;

            foreach (IRule rule in this)
            {
                IRule ruleResult = rule.Execute(obj);
                if (!ruleResult.IsValid) //validation failed
                {
                    isValid = false;
                    validationMessage += (string.IsNullOrWhiteSpace(ruleResult.ValidationMessage) ? "" : Environment.NewLine) +
                                         ruleResult.ValidationMessage;
                    if ((int)ruleResult.Severity > (int)highestSeverity)
                    {
                        highestSeverity = ruleResult.Severity;
                    }
                }
            }

            return new ValidationResult(isValid, highestSeverity, validationMessage);
        }

        //public void AddRule(Func<object, IRule> rule)
        //{
        //    this.Add(rule);
        //}

        public void AddRule(IRule rule)
        {
            this.Add(rule);
        }
    }
}
