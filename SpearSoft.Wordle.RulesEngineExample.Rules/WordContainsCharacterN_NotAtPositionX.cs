﻿using SpearSoft.SimpleRulesEngine.Rules;

namespace SpearSoft.Wordle.RulesEngineExample.Rules
{
    public class WordContainsCharacterN_NotAtPositionX:IRule
    {
        public char Character { get; private set; }
        public int Position { get; private set; }

        private WordContainsCharacterN_NotAtPositionX(char character, int position)
        {
            Character = character;
            Position = position;
        }

        public static WordContainsCharacterN_NotAtPositionX Create(char character, int position)
        {
            return new WordContainsCharacterN_NotAtPositionX(character, position);
        }

        public bool IsValid { get; set; } = true;
        public Severity Severity { get; set; } = Severity.Information;
        public RuleTypes RuleType { get; set; } = RuleTypes.LetterRequired;
        public string ValidationMessage { get; set; } = string.Empty;

        public IRule Execute(object target)
        {
            if (target is string mystring)
            {
                IsValid = true;
                ValidationMessage = string.Empty;

                if (!mystring.Contains(Character))
                {
                    IsValid = false;
                    ValidationMessage = $"Word does not contain character \"{Character}\" in any position.";
                }
                else if (mystring.Contains(Character) && mystring.IndexOf(Character) == Position - 1) //zero based index
                {
                    IsValid = false;
                    ValidationMessage = $"Word contain character \"{Character}\" but is not allowed at position {Position}.";
                }
            }

            return this;
        }
    }
}