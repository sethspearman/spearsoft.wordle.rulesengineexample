﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpearSoft.SimpleRulesEngine.Rules;

namespace SpearSoft.Wordle.RulesEngineExample.Rules
{
    public class WordHasExactlyNDistinctVowels : IRule
    {
        public int VowelCount { get; private set; }
        public bool ExcludeY { get; private set; }

        public WordHasExactlyNDistinctVowels(int vowelCount, bool excludeY)
        {
            VowelCount = vowelCount;
            ExcludeY = excludeY;
        }

        public static WordHasExactlyNDistinctVowels Create(int vowelCount, bool excludeY = false)
        {
            return new WordHasExactlyNDistinctVowels(vowelCount, excludeY);
        }

        public bool IsValid { get; set; } = true;
        public Severity Severity { get; set; } = Severity.Information;
        public RuleTypes RuleType { get; set; } = RuleTypes.Contains;
        public string ValidationMessage { get; set; } = string.Empty;
        public IRule Execute(object target)
        {
            if (target is string mystring)
            {
                IsValid = true;
                ValidationMessage = string.Empty;
                var vowelString = "AEIOU" + (ExcludeY ? "" : "Y");

                char[] vowels = vowelString.ToCharArray();

                List<char> foundVowels = new List<char>();

                
                foreach (char c in mystring)
                {
                    if (vowels.Contains(c) && !foundVowels.Contains(c))
                    {
                        foundVowels.Add(c);
                    }
                }

                var distinctVowelsCount = foundVowels.Count;
                
                if (distinctVowelsCount != VowelCount) //zero based index
                {
                    IsValid = false;
                    ValidationMessage = $"The word has {distinctVowelsCount} distinct vowel(s) not {VowelCount} vowel(s).";
                }
            }

            return this;
        }
    }
}
