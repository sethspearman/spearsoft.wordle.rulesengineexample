﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpearSoft.SimpleRulesEngine.Rules;

namespace SpearSoft.Wordle.RulesEngineExample.Rules
{
    public class WordHasExactlyNDistinctLetters : IRule
    {
        public int LetterCount { get; private set; }

        public WordHasExactlyNDistinctLetters(int letterCount)
        {
            LetterCount = letterCount;
        }

        public static WordHasExactlyNDistinctLetters Create(int letterCount)
        {
            return new WordHasExactlyNDistinctLetters(letterCount);
        }

        public bool IsValid { get; set; } = true;
        public Severity Severity { get; set; } = Severity.Information;
        public RuleTypes RuleType { get; set; } = RuleTypes.Contains;
        public string ValidationMessage { get; set; } = string.Empty;
        public IRule Execute(object target)
        {
            if (target is string mystring)
            {
                IsValid = true;
                ValidationMessage = string.Empty;

                var distinctLettersCount = mystring.Distinct().Count();

                if (distinctLettersCount != LetterCount) //zero based index
                {
                    IsValid = false;
                    ValidationMessage = $"The word has {distinctLettersCount} distinct letters(s) not {LetterCount} letters(s).";
                }
            }

            return this;
        }


    }
}
