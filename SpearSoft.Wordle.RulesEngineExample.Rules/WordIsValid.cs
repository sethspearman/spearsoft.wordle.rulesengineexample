using SpearSoft.SimpleRulesEngine.Rules;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpearSoft.Wordle.RulesEngineExample.Rules
{
    public class WordIsValid : IRule
    {
        private static IMasterWordList _wordList;
        private WordIsValid()
        {
        }

        public static WordIsValid Create()
        {
            _wordList = new MasterWordList();
            return new WordIsValid();
        }

        //for unit testing
        public static WordIsValid Create(IMasterWordList wordList)
        {
            _wordList = wordList;
            return new WordIsValid();
        }

        public bool IsValid { get; set; } = true;
        public Severity Severity { get; set; } = Severity.Stop;
        public RuleTypes RuleType { get; set; } = RuleTypes.IsValidWord;
        public string ValidationMessage { get; set; } = string.Empty;

        public IRule Execute(object target)
        {
            if (target is string mystring)
            {
                IsValid = true;
                ValidationMessage = string.Empty;

                if (!_wordList.IsInList(mystring))  //rule validation here.
                {
                    IsValid = false;
                    ValidationMessage = $"\"{mystring}\" is  not a valid word!";
                }
            }

            return this;
        }
    }
}