using SpearSoft.SimpleRulesEngine.Rules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpearSoft.Wordle.RulesEngineExample.Rules
{
    public class WordContainsCharacterN : IRule
    {
        public char Character { get; private set; }

        private WordContainsCharacterN(char character)
        {
            Character = character;
        }

        public static WordContainsCharacterN Create(char character)
        {
            return new WordContainsCharacterN(character);
        }

        public bool IsValid { get; set; } = true;
        public Severity Severity { get; set; } = Severity.Information;
        public RuleTypes RuleType { get; set; } = RuleTypes.LetterRequired;
        public string ValidationMessage { get; set; } = string.Empty;

        public IRule Execute(object target)
        {
            if (target is string mystring)
            {
                IsValid = true;
                ValidationMessage = string.Empty;

                if (!mystring.Contains(Character))
                {
                    IsValid = false;
                    ValidationMessage = $"Word does not contain the character \"{Character}\"";
                }
            }

            return this;
        }
    }
}