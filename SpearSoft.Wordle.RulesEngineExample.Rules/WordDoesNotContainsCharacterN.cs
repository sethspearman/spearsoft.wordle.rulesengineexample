using SpearSoft.SimpleRulesEngine.Rules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpearSoft.Wordle.RulesEngineExample.Rules
{
    public class WordDoesNotContainsCharacterN : IRule
    {
        public char Character { get; private set; }

        private WordDoesNotContainsCharacterN(char character)
        {
            Character = character;
        }

        public static WordDoesNotContainsCharacterN Create(char character)
        {
            return new WordDoesNotContainsCharacterN(character);
        }

        public bool IsValid { get; set; } = true;
        public Severity Severity { get; set; } = Severity.Information;
        public RuleTypes RuleType { get; set; } = RuleTypes.LetterNotAllowed;
        public string ValidationMessage { get; set; } = string.Empty;

        public IRule Execute(object target)
        {
            if (target is string mystring)
            {
                IsValid = true;
                ValidationMessage = string.Empty;

                if (mystring.Contains(Character))
                {
                    IsValid = false;
                    ValidationMessage = $"Word should not contain character \"{Character}\".";
                }
            }

            return this;
        }
    }
}