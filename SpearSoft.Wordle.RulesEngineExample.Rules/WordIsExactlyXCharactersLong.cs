﻿using SpearSoft.SimpleRulesEngine.Rules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpearSoft.Wordle.RulesEngineExample.Rules
{

    public class WordIsExactlyXCharactersLong : IRule
    {
        public int WordLength { get; private set; }

        private WordIsExactlyXCharactersLong(byte wordLength = 5)
        {
            WordLength = wordLength;
        }

        public static WordIsExactlyXCharactersLong Create(byte wordLength)
        {
            return new WordIsExactlyXCharactersLong(wordLength);
        }

        public bool IsValid { get; set; } = true;
        public Severity Severity { get; set; } = Severity.Stop;
        public RuleTypes RuleType { get; set; } = RuleTypes.WordLength;
        public string ValidationMessage { get; set; } = string.Empty;
        public IRule Execute(object target)
        {
            //you can overwrite the initialized properties here if you like.  but I am not in this case.
            //Example:
            //Severity = Severity.None;

            if (target is string mystring)
            {
                IsValid = true;
                ValidationMessage = string.Empty;

                var actualWordLength = mystring.Length;
                if (actualWordLength!= WordLength)
                {
                    IsValid = false;
                    ValidationMessage = $"Word was expected to be {WordLength} long but instead was {actualWordLength} long.";

                }
            }

            return this;
        }
    }
}
