﻿using System.Collections.Generic;

namespace SpearSoft.Wordle.RulesEngineExample.Rules
{
    public interface IMasterWordList
    {
        List<string> WordList { get; set; }
        bool IsInList(string WordToSearch);
    }
}