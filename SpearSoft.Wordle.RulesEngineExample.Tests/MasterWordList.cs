﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpearSoft.Wordle.RulesEngineExample.Rules;

namespace SpearSoft.Wordle.RulesEngineExample.Tests
{
    public class MasterWordList : IMasterWordList
    {
        public List<string> WordList { get; set; } = new List<string>();

        public bool IsInList(string WordToSearch)
        {
            //(MasterWordList.WordList.BinarySearch(mystring.ToUpper(),null)>=0)

            return WordList.BinarySearch(WordToSearch, null) >= 0;

        }

        public MasterWordList()
        {
            if (WordList.Count == 0)
            {
                WordList = GetWordList();
            }
        }
        private static List<string> GetWordList()
        {
            var result = new List<string>();

            result.Add("ABACK");
            result.Add("AMUSE");
            result.Add("AVAIL");
            result.Add("AVERT");
            result.Add("BLOAT");
            result.Add("BRICK");
            result.Add("BURNT");
            result.Add("CONCH");
            result.Add("DOLLY");
            result.Add("ETHER");
            result.Add("ETHIC");
            result.Add("ETHOS");
            result.Add("FIELD");
            result.Add("GROVE");
            result.Add("HOARD");
            result.Add("HUSKY");
            result.Add("INTER");
            result.Add("LOSER");
            result.Add("LOUSE");
            result.Add("NUTTY");
            result.Add("REVUE");
            result.Add("SIXTH");
            result.Add("SUSHI");
            result.Add("AISLE");


            return result;
        }
    }

}
