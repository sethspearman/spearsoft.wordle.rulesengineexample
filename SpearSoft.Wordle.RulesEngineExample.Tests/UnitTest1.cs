using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using NUnit.Framework;
using SpearSoft.SimpleRulesEngine.Rules;
using SpearSoft.Wordle.RulesEngineExample.Rules;

namespace SpearSoft.Wordle.RulesEngineExample.Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [TearDown]
        public void Teardown()
        {
        }

        [Test]
        public void Rules_CanBeCombined_True()
        {
            //arrange
            //create limited work list
            var testWord = "BLOAT";

            var rules = new RulesCollection();

            rules.AddRule(WordContainsCharacterN.Create('L'));
            rules.AddRule(WordIsValid.Create(new MasterWordList()));

            //act
            var result = rules.CheckRules(testWord);

            //assert

            Assert.IsTrue(result.IsValid && result.ValidationMessages==string.Empty);
        }

        [Test]
        public void Rules_WordContainsCharacter_False()
        {
            //arrange
            //create limited work list
            var testWord = "BLOAT";

            var rules = new RulesCollection();

            rules.AddRule(WordContainsCharacterN.Create('Z'));

            //act
            var result = rules.CheckRules(testWord);

            //assert

            Assert.IsFalse(result.IsValid);
            Assert.IsTrue(result.ValidationMessages.Contains("Word does not contain the character"));
        }
        [Test]
        public void Rules_WordContainsCharacter_True()
        {
            //arrange
            //create limited work list
            var testWord = "BLOAT";
            
            var rules = new RulesCollection();

            rules.AddRule(WordContainsCharacterN.Create('L'));

            //act
            var result = rules.CheckRules(testWord);

            //assert

            Assert.IsTrue(result.IsValid);
            Assert.IsTrue(string.IsNullOrWhiteSpace(result.ValidationMessages));
        }

        [Test]
        public void Rules_WordDoesNotContainsCharacter_True()
        {
            //arrange
            //create limited work list
            var testWord = "BLOAT";

            var rules = new RulesCollection();

            rules.AddRule(WordDoesNotContainsCharacterN.Create('Z'));

            //act
            var result = rules.CheckRules(testWord);

            //assert

            Assert.IsTrue(result.IsValid);
            Assert.IsTrue(string.IsNullOrWhiteSpace(result.ValidationMessages));
        }
        [Test]
        public void Rules_WordDoesNotContainsCharacter_False()
        {
            //arrange
            //create limited work list
            var testWord = "BLOAT";

            var rules = new RulesCollection();

            rules.AddRule(WordDoesNotContainsCharacterN.Create('L'));


            //act
            var result = rules.CheckRules(testWord);

            //assert

            Assert.IsFalse(result.IsValid);
            Assert.IsTrue(result.ValidationMessages.Contains("Word should not contain character"));
        }
        [Test]
        public void Rules_WordContains_Character_at_Pos_N_True()
        {
            //arrange
            //create limited work list
            var testWord = "BLOAT";

            var rules = new RulesCollection();

            rules.AddRule(WordContainsCharacterN_AtPositionX.Create('L',2));

            //act
            var result = rules.CheckRules(testWord);

            //assert

            Assert.IsTrue(result.IsValid);
            Assert.IsTrue(string.IsNullOrWhiteSpace(result.ValidationMessages));
        }
        [Test]
        public void Rules_WordContains_Character_at_Pos_N_False()
        {
            //arrange
            //create limited work list
            var testWord = "MERGE";

            var rules = new RulesCollection();

            rules.AddRule(WordContainsCharacterN_AtPositionX.Create('L',2));


            //act
            var result = rules.CheckRules(testWord);

            //assert

            Assert.IsFalse(result.IsValid);
            Assert.IsTrue(result.ValidationMessages.Contains("Word does not contain character"));
        }
        [Test]
        public void Rules_Word_Is_X_Chars_Long_True()
        {
            //arrange
            //create limited work list
            var testWord = "BLOAT";

            var rules = new RulesCollection();

            rules.AddRule(WordIsExactlyXCharactersLong.Create(5));

            //act
            var result = rules.CheckRules(testWord);

            //assert

            Assert.IsTrue(result.IsValid);
            Assert.IsTrue(string.IsNullOrWhiteSpace(result.ValidationMessages));
        }
        [Test]
        public void Rules_Word_Is_X_Chars_Long_False()
        {
            //arrange
            //create limited work list
            var testWord = "BLOAT";

            var rules = new RulesCollection();

            rules.AddRule(WordIsExactlyXCharactersLong.Create(2));


            //act
            var result = rules.CheckRules(testWord);

            //assert

            Assert.IsFalse(result.IsValid);
            Assert.IsTrue(result.ValidationMessages.Contains("Word was expected to be"));
        }

        [Test]
        public void Rules_Word_Is_Valid_True()
        {
            //arrange
            //create limited work list
            var testWord = "BLOAT";

            var rules = new RulesCollection();

            rules.AddRule(WordIsValid.Create(new MasterWordList()));

            //act
            var result = rules.CheckRules(testWord);

            //assert

            Assert.IsTrue(result.IsValid);
            Assert.IsTrue(string.IsNullOrWhiteSpace(result.ValidationMessages));
        }
        [Test]
        public void Rules_Word_Is_Valid_False()
        {
            //arrange
            //create limited work list
            var testWord = "MAINS";

            var rules = new RulesCollection();

            rules.AddRule(WordIsValid.Create(new MasterWordList()));

            //act
            var result = rules.CheckRules(testWord);

            //assert

            Assert.IsFalse(result.IsValid);
            Assert.IsTrue(result.ValidationMessages.Contains("is  not a valid word"));
        }

        [Test]
        public void Can_Emulate_Game_true()
        {
            var todaysWord = "ETHER";

            var wordList = new MasterWordList();



            var rules = new RulesCollection();
            rules.Add(WordIsExactlyXCharactersLong.Create(5));

            var guess1 = "NUTTY";
            rules.AddRule(WordDoesNotContainsCharacterN.Create('N'));
            rules.AddRule(WordDoesNotContainsCharacterN.Create('U'));
            rules.AddRule(WordDoesNotContainsCharacterN.Create('Y'));
            rules.AddRule(WordContainsCharacterN.Create('T'));

            //OutputResults(wordList, rules);

            var guess2 = "HOARD";
            rules.AddRule(WordDoesNotContainsCharacterN.Create('O'));
            rules.AddRule(WordDoesNotContainsCharacterN.Create('A'));
            rules.AddRule(WordDoesNotContainsCharacterN.Create('D'));
            rules.AddRule(WordContainsCharacterN.Create('H'));
            rules.AddRule(WordContainsCharacterN.Create('R'));

            //OutputResults(wordList, rules);


            var guess3 = "ETHIC";
            rules.AddRule(WordContainsCharacterN_AtPositionX.Create('E',1));
            rules.AddRule(WordContainsCharacterN_AtPositionX.Create('T',2));
            rules.AddRule(WordContainsCharacterN_AtPositionX.Create('H',3));
            rules.AddRule(WordDoesNotContainsCharacterN.Create('I'));
            rules.AddRule(WordDoesNotContainsCharacterN.Create('C'));
            
            //OutputResults(wordList, rules);


            Assert.Pass();
        }

        [Test]
        public void Can_Get_Distinct_Letters_And_Vowels()
        {
            //var wordList = new MasterWordList();
            var wordList = new SpearSoft.Wordle.RulesEngineExample.Rules.MasterWordList();

            var rules = new RulesCollection();
            rules.AddRule(WordHasExactlyNDistinctVowels.Create(3));
            rules.AddRule(WordHasExactlyNDistinctLetters.Create(5));

            var resultsList = OutputResults(wordList, rules);
            Assert.IsTrue(resultsList.Count > 0);
        }

        [Test]
        public void Can_Emulate_Game_2_true()
        {
            var wordList = new SpearSoft.Wordle.RulesEngineExample.Rules.MasterWordList();   //MasterWordList();
            //var wordList = new MasterWordList();



            var rules = new RulesCollection();
            rules.Add(WordIsExactlyXCharactersLong.Create(5));

            rules.AddRule(WordDoesNotContainsCharacterN.Create('S'));
            rules.AddRule(WordDoesNotContainsCharacterN.Create('A'));
            rules.AddRule(WordDoesNotContainsCharacterN.Create('U'));
            rules.AddRule(WordDoesNotContainsCharacterN.Create('T'));
            rules.AddRule(WordDoesNotContainsCharacterN.Create('E'));
            rules.AddRule(WordDoesNotContainsCharacterN.Create('V'));
            rules.AddRule(WordDoesNotContainsCharacterN.Create('L'));
            //rules.AddRule(WordDoesNotContainsCharacterN.Create('H'));
            //rules.AddRule(WordDoesNotContainsCharacterN.Create('U'));
            //rules.AddRule(WordDoesNotContainsCharacterN.Create('W'));
            
            rules.AddRule(WordContainsCharacterN_AtPositionX.Create('P', 1));
            rules.AddRule(WordContainsCharacterN_AtPositionX.Create('R', 2));
            rules.AddRule(WordContainsCharacterN_AtPositionX.Create('I', 3));
            //rules.AddRule(WordContainsCharacterN_AtPositionX.Create('O', 3));
            //rules.AddRule(WordContainsCharacterN_AtPositionX.Create('E', 5));
            //rules.AddRule(WordContainsCharacterN_AtPositionX.Create('O', 3));
            rules.AddRule(WordContainsCharacterN_NotAtPositionX.Create('O', 2));
            rules.AddRule(WordContainsCharacterN_NotAtPositionX.Create('O', 4));
            //rules.AddRule(WordContainsCharacterN_NotAtPositionX.Create('E', 2));
            //rules.AddRule(WordContainsCharacterN_NotAtPositionX.Create('E', 4));
            //rules.AddRule(WordContainsCharacterN_NotAtPositionX.Create('A', 3));
            //rules.AddRule(WordContainsCharacterN_NotAtPositionX.Create('R', 5));
            

            var resultsList = OutputResults(wordList, rules);


            Assert.IsTrue(resultsList.Count>0);
            //Assert.IsTrue(resultsList.Contains("CARVE"));
            //Assert.IsTrue(resultsList.Contains("FARCE"));
        }

        private static List<string> OutputResults(MasterWordList wordList, RulesCollection rules)
        {
            var validWords = new List<string>();

            foreach (var word in wordList.WordList)
            {
                var result = rules.CheckRules(word);
                Console.WriteLine($"{word} IsValid: {result.IsValid}:  {result.ValidationMessages}");
                if (result.IsValid)
                {
                    validWords.Add(word);
                }
            }

            Console.WriteLine("***************************************");
            foreach (var word in validWords)
            {
                Console.WriteLine(word);
            }
            Console.WriteLine("***************************************");

            return validWords;

        }
        private static List<string> OutputResults(SpearSoft.Wordle.RulesEngineExample.Rules.MasterWordList wordList, RulesCollection rules)
        {
            var validWords = new List<string>();

            foreach (var word in wordList.WordList)
            {
                var result = rules.CheckRules(word);
                Console.WriteLine($"{word} IsValid: {result.IsValid}:  {result.ValidationMessages}");
                if (result.IsValid)
                {
                    validWords.Add(word);
                }
            }

            Console.WriteLine("***************************************");
            foreach (var word in validWords)
            {
                Console.WriteLine(word);
            }
            Console.WriteLine("***************************************");

            return validWords;

        }
    }
}