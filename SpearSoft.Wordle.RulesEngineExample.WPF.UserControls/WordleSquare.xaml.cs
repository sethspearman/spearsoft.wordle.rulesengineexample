﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.RightsManagement;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Color = System.Drawing.Color;

namespace wordle_controls
{
    /// <summary>
    /// Interaction logic for WordleSquare.xaml
    /// </summary>
    public partial class WordleSquare : UserControl
    {
        public static readonly DependencyProperty SquareStatusProperty = DependencyProperty.RegisterAttached("SquareStatus", typeof(object), typeof(WordleSquare), new PropertyMetadata(default(object)));


        public string Text
        {
            get => Square.Text;
            set => Square.Text = value.ToUpper();
        }

        public SquareStatus SquareStatus { get; set; } = SquareStatus.Undecided;

        public SolidColorBrush CurrentColor { get; set; } = Brushes.Black;

        public WordleSquare()
        {
            InitializeComponent();

            Square.Background = CurrentColor;

        }

        private void Square_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            

            Regex regex = new Regex("[A-Za-z]");
            
            e.Handled = !regex.IsMatch(e.Text);
        }

        private void Square_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            OnPreviewKeyDown(e);
        }

        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;
            }
            base.OnPreviewKeyDown(e);
        }

        private void Square_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            switch (SquareStatus)
            {
                case SquareStatus.Undecided:
                    SquareStatus = SquareStatus.NotInWord;
                    break;
                case SquareStatus.NotInWord:
                    SquareStatus = SquareStatus.IsInWord;
                    break;
                case SquareStatus.IsInWord:
                    SquareStatus = SquareStatus.IsInPlace;
                    break;
                case SquareStatus.IsInPlace:
                    SquareStatus = SquareStatus.Undecided;
                    break;
            }

            SetSquareBackground(SquareStatus);
        }


        private void SetSquareBackground(SquareStatus status)
        {
            var color = GetSolidColorBrush(status);
            Square.Background = color;

            if (color == Brushes.Yellow)
            {
                Square.Foreground = Brushes.Black;
            }
            else
            {
                Square.Foreground = Brushes.White;
            }
        }


        private SolidColorBrush GetSolidColorBrush(SquareStatus status)
        {
            var result = new SolidColorBrush();

            switch (status)
            {
                case SquareStatus.Undecided:
                    result = Brushes.Black;
                    break;
                case SquareStatus.IsInPlace:
                    result = Brushes.Green;
                    break;
                case SquareStatus.IsInWord:
                    result = Brushes.Yellow;
                    break;
                case SquareStatus.NotInWord:
                    result = Brushes.DimGray;
                    break;
                default:
                    result = Brushes.Black;
                    break;
            }

            return result;
        }
    }

    public enum SquareStatus
    {
        Undecided = 0,
        NotInWord = 1,
        IsInWord = 2,
        IsInPlace = 3
    }


}
